import axios from 'axios';

function create(episode) {
  return axios
    .post(
      process.env.REACT_APP_API_KEY + "/episodes",
      episode
    );
}

function findAll() {
  return axios
    .get(process.env.REACT_APP_API_KEY + "/episodes/?pagination=false")
    .then((response) => response.data["hydra:member"]);
}

function find(id) {
  return axios
    .get(process.env.REACT_APP_API_KEY + "/episodes/" + id)
    .then((response) => response.data);
}

function update(id, episode) {
  return axios
    .put(
      process.env.REACT_APP_API_KEY + "/episodes/" + id,
      episode
    );
}

function deleteEpisode(id) {
  return axios
    .delete(process.env.REACT_APP_API_KEY + "/episodes/" + id)
}

export default {
  create,
  findAll,
  find,
  update,
  delete: deleteEpisode
}