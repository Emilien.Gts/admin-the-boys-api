import axios from 'axios'
import jwtDecode from 'jwt-decode'

function authenticate(credentials) {
  return axios
    .post(process.env.REACT_APP_API_KEY + "/login_check", credentials)
    .then((response) => response.data.token)
    .then(token => {
      window.localStorage.setItem("authToken", token);
      setAxiosToken(token)
      return true;
    })
}

function logout() {
  window.localStorage.removeItem('authToken');
  delete axios.defaults.headers['Authorization']
}

function setAxiosToken(token) {
  axios.defaults.headers["Authorization"] = "Bearer " + token;
}

function setup() {
  const token = window.localStorage.getItem('authToken')

  if (token) {
    const { exp } = jwtDecode(token)
    if (exp * 1000 > new Date().getTime()) {
      setAxiosToken(token)
    }
  }
}

function isAuthenticated() {
  const token = window.localStorage.getItem('authToken')

  if (token) {
    const { exp } = jwtDecode(token)
    if (exp * 1000 > new Date().getTime()) {
      return true
    } else {
      return false
    }
  } else {
    return false
  }
}

export default {
  authenticate,
  logout,
  setup,
  isAuthenticated
}