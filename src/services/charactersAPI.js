import axios from 'axios';

function create(character) {
  return axios
    .post(
      process.env.REACT_APP_API_KEY + "/characters",
      character
    );
}

function findAll() {
  return axios
    .get(process.env.REACT_APP_API_KEY + "/characters/?pagination=false")
    .then((response) => response.data["hydra:member"]);
}

function find(id) {
  return axios
    .get(process.env.REACT_APP_API_KEY + "/characters/" + id)
    .then((response) => response.data);
}

function update(id, character) {
  return axios
    .put(
      process.env.REACT_APP_API_KEY + "/characters/" + id,
      character
    );
}

function deleteCharacter(id) {
  return axios
    .delete(process.env.REACT_APP_API_KEY + "/characters/" + id)
}

export default {
  create,
  findAll,
  find,
  update,
  delete: deleteCharacter
}