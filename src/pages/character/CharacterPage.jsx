import React, { useState, useEffect } from "react";
import Input from "../../components/Input";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import CharactersAPI from "../../services/charactersAPI";
import FormLoader from "../../components/Loaders/FormLoader";

const CharacterPage = ({ match, history }) => {
  const { id = "new" } = match.params;

  const [character, setCharacter] = useState({
    name: "",
    image: "",
    realName: "",
    specie: "",
    citizenship: "",
    gender: "",
    status: "",
    description: "",
  });

  const [errors, setErrors] = useState({
    name: "",
    realName: "",
    specie: "",
    citizenship: "",
    gender: "",
    status: "",
  });

  const [editing, setEditing] = useState(false);
  const [loading, setLoading] = useState(true);

  const fetchCharacter = async (id) => {
    try {
      const {
        name,
        image,
        realName,
        specie,
        citizenship,
        gender,
        status,
        description,
      } = await CharactersAPI.find(id);
      setCharacter({
        name,
        image,
        realName,
        specie,
        citizenship,
        gender,
        status,
        description,
      });
      setLoading(false);
    } catch (error) {
      toast.error("An error has occurred");
      console.log(error);
      history.replace("/characters");
    }
  };

  useEffect(() => {
    if (id !== "new") {
      setEditing(true);
      fetchCharacter(id);
    } else {
      setLoading(false);
    }
  }, [id]);

  const handleChange = ({ currentTarget }) => {
    const { name, value } = currentTarget;
    setCharacter({ ...character, [name]: value });
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    try {
      if (editing) {
        await CharactersAPI.update(id, character);
        toast.success("Charcater has been modify");
      } else {
        await CharactersAPI.create(character);
        toast.success("Character has been create");
        history.replace("/characters");
      }
    } catch ({ response }) {
      const { violations } = response.data;
      if (violations.length >= 1) {
        const apiErrors = {};
        violations.map(({ propertyPath, message }) => {
          apiErrors[propertyPath] = message;
        });
        toast.error("An error has occurred");
        setErrors(apiErrors);
      }
    }
  };

  return (
    <>
      {editing ? <h1>Modify character</h1> : <h1>Set new Character</h1>}

      {!loading && (
        <form onSubmit={handleSubmit}>
          <Input
            name="name"
            label="Name"
            placeholder="Name of character"
            value={character.name}
            required={true}
            onChange={handleChange}
            error={errors.name}
          />
          <Input
            name="image"
            label="Image of character"
            placeholder="Image of character"
            value={character.image}
            onChange={handleChange}
          />
          <Input
            name="realName"
            label="Real name"
            placeholder="Real name of character"
            required={true}
            value={character.realName}
            onChange={handleChange}
            error={errors.realName}
          />
          <Input
            name="specie"
            label="Specie"
            placeholder="Specie of character"
            required={true}
            value={character.specie}
            onChange={handleChange}
            error={errors.specie}
          />
          <Input
            name="citizenship"
            label="Citizenship"
            placeholder="Citizenship of character"
            required={true}
            value={character.citizenship}
            onChange={handleChange}
            error={errors.citizenship}
          />
          <Input
            name="gender"
            label="Gender"
            placeholder="Gender of character"
            required={true}
            value={character.gender}
            onChange={handleChange}
            error={errors.gender}
          />
          <Input
            name="status"
            label="Status"
            placeholder="Status of character"
            required={true}
            value={character.status}
            onChange={handleChange}
            error={errors.status}
          />
          <div className="form-group">
            <label htmlFor="description">Description</label>
            <textarea
              name="description"
              className="form-control"
              id="description"
              rows="3"
              placeholder="Description of character"
              value={character.description}
              onChange={handleChange}
            ></textarea>
          </div>

          <div className="form-group">
            <button className="btn btn-success">Save new character</button>
            <Link to="/characters" className="btn btn-link">
              Retour à la liste
            </Link>
          </div>
        </form>
      )}

      {loading && <FormLoader />}
    </>
  );
};

export default CharacterPage;
