import React, { useEffect, useState } from "react";
import Pagination from "../../components/Pagination";
import CharactersAPI from "../../services/charactersAPI.js";
import { Link } from "react-router-dom";
import TableLoader from "../../components/Loaders/TableLoader";

const CharactersPage = () => {
  const [characters, setCharacters] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [search, setSearch] = useState("");
  const [loading, setLoading] = useState(true);

  const itemsPerPage = 15;
  const filteredCharacters = characters.filter((c) =>
    c.realName.toLowerCase().includes(search.toLowerCase())
  );
  const paginatedCharacters = Pagination.getData(
    filteredCharacters,
    currentPage,
    itemsPerPage
  );

  const fetchCharacters = async () => {
    try {
      const data = await CharactersAPI.findAll();
      setCharacters(data);
      setLoading(false);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    fetchCharacters();
  }, []);

  const handleDelete = async (character) => {
    const originalCharacters = [...characters];
    setCharacters(characters.filter((c) => c.id !== character.id));

    if (
      character.affiliations.length !== 0 ||
      character.aliases.length !== 0 ||
      character.episodes.length !== 0 ||
      character.familyMembers.length !== 0 ||
      character.occupations.length !== 0 ||
      character.powers.length !== 0
    ) {
      alert("This character has some dependencies, delete them first");
    } else {
      try {
        await CharactersAPI.delete(character.id);
      } catch (error) {
        setCharacters(originalCharacters);
        console.log(error);
      }
    }
  };

  const handlePageChange = (page) => {
    setCurrentPage(page);
  };

  const handleSearch = ({ currentTarget }) => {
    setSearch(currentTarget.value);
    setCurrentPage(1);
  };

  return (
    <>
      <div className="d-flex justify-content-between align-items-center mb-3">
        <h1>Characters</h1>
        <Link to="/characters/new/" className="btn btn-primary">
          Create a new character
        </Link>
      </div>

      <div className="form-group">
        <input
          type="text"
          onChange={handleSearch}
          value={search}
          className="form-control"
          placeholder="Rechercher..."
        />
      </div>

      <table className="table table-hover">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Specie</th>
            <th>Citizenship</th>
            <th>Gender</th>
            <th>Status</th>
            <th />
          </tr>
        </thead>

        {!loading && (
          <tbody>
            {paginatedCharacters.map((character) => (
              <tr key={character.id}>
                <td>{character.id}</td>
                <td>
                  <Link
                    to={"/characters/" + character.id}
                    className="btn btn-link"
                  >
                    {character.realName}
                  </Link>
                </td>
                <td>{character.specie}</td>
                <td>{character.citizenship}</td>
                <td>{character.gender}</td>
                <td>{character.status}</td>
                <td>
                  <button
                    onClick={() => handleDelete(character)}
                    className="btn btn-sm btn-danger"
                  >
                    Delete
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        )}
      </table>

      {loading && <TableLoader />}

      {itemsPerPage < filteredCharacters.length && (
        <Pagination
          currentPage={currentPage}
          itemsPerPage={itemsPerPage}
          length={filteredCharacters.length}
          onPageChanged={handlePageChange}
        />
      )}
    </>
  );
};

export default CharactersPage;
