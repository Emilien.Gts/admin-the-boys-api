import React, { useState, useEffect } from "react";
import Input from "../../components/Input";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import EpisodesAPI from "../../services/episodesAPI";
import FormLoader from "../../components/Loaders/FormLoader";

const EpisodePage = ({ match, history }) => {
  const { id = "new" } = match.params;

  const [episode, setEpisode] = useState({
    name: "",
    seasonNumber: 0,
    episodeNumber: 0,
    releaseDate: 0,
    summary: "",
  });

  const [errors, setErrors] = useState({
    name: "",
    seasonNumber: "",
    episodeNumber: "",
    releaseDate: "",
    summary: "",
  });

  const [editing, setEditing] = useState(false);
  const [loading, setLoading] = useState(true);

  const fetchEpisode = async (id) => {
    try {
      const {
        name,
        seasonNumber,
        episodeNumber,
        releaseDate,
        summary,
      } = await EpisodesAPI.find(id);
      setEpisode({
        name,
        seasonNumber,
        episodeNumber,
        releaseDate,
        summary,
      });
      setLoading(false);
    } catch (error) {
      toast.error("An error has occurred");
      console.log(error);
      history.replace("/episodes");
    }
  };

  useEffect(() => {
    if (id !== "new") {
      setEditing(true);
      fetchEpisode(id);
    } else {
      setLoading(false);
    }
  }, [id]);

  const handleChange = ({ currentTarget }) => {
    const { name, value } = currentTarget;
    name === "seasonNumber" || name === "episodeNumber"
      ? setEpisode({ ...episode, [name]: parseInt(value) })
      : setEpisode({ ...episode, [name]: value });
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    console.log(episode);

    try {
      if (editing) {
        await EpisodesAPI.update(id, episode);
        toast.success("Episode has been modify");
      } else {
        await EpisodesAPI.create(episode);
        toast.success("Episode has been create");
        history.replace("/episodes");
      }
    } catch ({ response }) {
      console.log(response.data);
      // const { violations } = response.data;
      // if (violations.length >= 1) {
      //   const apiErrors = {};
      //   violations.map(({ propertyPath, message }) => {
      //     apiErrors[propertyPath] = message;
      //   });
      //   toast.error("An error has occurred");
      //   setErrors(apiErrors);
      // }
    }
  };

  return (
    <>
      {editing ? <h1>Modify episode</h1> : <h1>Set new Episode</h1>}

      {!loading && (
        <form onSubmit={handleSubmit}>
          <Input
            name="name"
            label="Name"
            placeholder="Name of episode"
            value={episode.name}
            required={true}
            onChange={handleChange}
            error={errors.name}
          />
          <Input
            name="seasonNumber"
            label="seasonNumber"
            placeholder="Season number of episode"
            value={episode.seasonNumber}
            required={true}
            onChange={handleChange}
            error={errors.seasonNumber}
            type="number"
          />
          <Input
            name="episodeNumber"
            label="episodeNumber"
            placeholder="Episode number of episode"
            value={episode.episodeNumber}
            required={true}
            onChange={handleChange}
            error={errors.episodeNumber}
            type="number"
          />
          <Input
            name="releaseDate"
            label="releaseDate"
            placeholder="Release date of episode"
            value={episode.releaseDate}
            required={true}
            onChange={handleChange}
            error={errors.releaseDate}
            type="date"
          />
          <div className="form-group">
            <label htmlFor="summary">Summary</label>
            <textarea
              name="summary"
              className="form-control"
              id="summary"
              rows="3"
              placeholder="Summary of episode"
              value={episode.summary}
              onChange={handleChange}
            ></textarea>
          </div>

          <div className="form-group">
            <button className="btn btn-success">Save new episode</button>
            <Link to="/episodes" className="btn btn-link">
              return to all episodes list
            </Link>
          </div>
        </form>
      )}

      {loading && <FormLoader />}
    </>
  );
};

export default EpisodePage;
