import React, { useEffect, useState } from "react";
import Pagination from "../../components/Pagination";
import EpisodesAPI from "../../services/episodesAPI.js";
import { Link } from "react-router-dom";
import TableLoader from "../../components/Loaders/TableLoader";

const EpisodesPage = () => {
  const [episodes, setEpisodes] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [search, setSearch] = useState("");
  const [loading, setLoading] = useState(true);

  const itemsPerPage = 15;
  const filteredEpisodes = episodes.filter((e) =>
    e.name.toLowerCase().includes(search.toLowerCase())
  );
  const paginatedEpisodes = Pagination.getData(
    filteredEpisodes,
    currentPage,
    itemsPerPage
  );

  const fetchEpisodes = async () => {
    try {
      const data = await EpisodesAPI.findAll();
      setEpisodes(data);
      setLoading(false);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    fetchEpisodes();
  }, []);

  const handleDelete = async (episode) => {
    const originalEpisodes = [...episodes];
    setEpisodes(episodes.filter((e) => e.id !== episode.id));

    if (
      episode.cast.length !== 0 ||
      episode.castFirstSeen.length !== 0 ||
      episode.castLastSeen.length !== 0
    ) {
      alert("This episode has some dependencies, delete them first");
    } else {
      try {
        await EpisodesAPI.delete(episode.id);
      } catch (error) {
        setEpisodes(originalEpisodes);
        console.log(error);
      }
    }
  };

  const handlePageChange = (page) => {
    setCurrentPage(page);
  };

  const handleSearch = ({ currentTarget }) => {
    setSearch(currentTarget.value);
    setCurrentPage(1);
  };

  return (
    <>
      <div className="d-flex justify-content-between align-items-center mb-3">
        <h1>Episodes</h1>
        <Link to="/episode/new/" className="btn btn-primary">
          Create a new episode
        </Link>
      </div>

      <div className="form-group">
        <input
          type="text"
          onChange={handleSearch}
          value={search}
          className="form-control"
          placeholder="Rechercher..."
        />
      </div>

      <table className="table table-hover">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Season Numb.</th>
            <th>Episode Numb.</th>
            <th>RealeaseDate</th>
            <th />
          </tr>
        </thead>

        {!loading && (
          <tbody>
            {paginatedEpisodes.map((episode) => (
              <tr key={episode.id}>
                <td>{episode.id}</td>
                <td>
                  <Link to={"/episode/" + episode.id} className="btn btn-link">
                    {episode.name}
                  </Link>
                </td>
                <td>{episode.seasonNumber}</td>
                <td>{episode.episodeNumber}</td>
                <td>{episode.releaseDate}</td>
                <td>
                  <button
                    onClick={() => handleDelete(episode)}
                    className="btn btn-sm btn-danger"
                  >
                    Delete
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        )}
      </table>

      {loading && <TableLoader />}

      {itemsPerPage < filteredEpisodes.length && (
        <Pagination
          currentPage={currentPage}
          itemsPerPage={itemsPerPage}
          length={filteredEpisodes.length}
          onPageChanged={handlePageChange}
        />
      )}
    </>
  );
};

export default EpisodesPage;
