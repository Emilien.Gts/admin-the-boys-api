import React from "react";

const HomePage = () => {
  return (
    <div className="jumbotron">
      <h1 className="display-3">Admin API The Boys</h1>
      <p className="lead">
        Admin part of The Boys API, allowing entity management
      </p>
      <hr className="my-4" />
      <p>This part is reserved exclusively for API administrators.</p>
    </div>
  );
};

export default HomePage;
