import React from "react";

const Input = ({
  name,
  label,
  value,
  onChange,
  placeholder = "",
  required = false,
  type = "text",
  error = "",
}) => {
  return (
    <div className="form-group">
      <label htmlFor={name}>{label}</label>
      <input
        type={type}
        name={name}
        className="form-control"
        id={name}
        placeholder={placeholder || label}
        required={required ? "required" : ""}
        value={value}
        onChange={onChange}
      />
      {error && (
        <p className="invalid-feedback" style={{ display: "block" }}>
          {error}
        </p>
      )}
    </div>
  );
};

export default Input;
