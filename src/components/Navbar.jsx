import React, { useContext } from "react";
import AuthAPI from "./../services/authAPI";
import { NavLink } from "react-router-dom";
import AuthContext from "./../contexts/AuthContext";
import { toast } from "react-toastify";

const Navbar = ({ history }) => {
  const { isAuthenticated, setIsAuthenticated } = useContext(AuthContext);
  console.log(isAuthenticated);

  const handleLogout = () => {
    AuthAPI.logout();
    setIsAuthenticated(false);
    toast.info("You are disconnected !");
    history.push("/login");
  };

  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
      <NavLink className="navbar-brand" to="/">
        Admin API The Boys
      </NavLink>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarColor01"
        aria-controls="navbarColor01"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon"></span>
      </button>

      <div className="collapse navbar-collapse" id="navbarColor01">
        <ul className="navbar-nav mr-auto">
          <li className="nav-item active">
            <NavLink className="nav-link" to="/">
              Home
              <span className="sr-only">(current)</span>
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/characters">
              Characters
              <span className="sr-only">(current)</span>
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" to="/episodes">
              Episodes
              <span className="sr-only">(current)</span>
            </NavLink>
          </li>
        </ul>

        <ul className="navbar-nav ml-auto">
          {!isAuthenticated && (
            <li className="nav-item">
              <NavLink to="/login" className="btn btn-success">
                Login
              </NavLink>
            </li>
          )}
          {isAuthenticated && (
            <li className="nav-item ml-3">
              <button onClick={handleLogout} className="btn btn-danger">
                Logout
              </button>
            </li>
          )}
        </ul>
      </div>
    </nav>
  );
};

export default Navbar;
