import "bootswatch/dist/litera/bootstrap.min.css";
import React, { useState } from 'react'
import Navbar from "./components/Navbar";
import HomePage from "./pages/HomePage";
import { HashRouter, Switch, Route, withRouter } from "react-router-dom";
import CharactersPage from "./pages/character/CharactersPage";
import CharacterPage from "./pages/character/CharacterPage";
import EpisodesPage from "./pages/episode/EpisodesPage";
import EpisodePage from "./pages/episode/EpisodePage";
import LoginPage from "./pages/LoginPage"
import AuthAPI from './services/authAPI'
import AuthContext from './contexts/AuthContext'
import PrivateRoute from "./components/PrivateRoute";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


AuthAPI.setup();



function App() {
  const [isAuthenticated, setIsAuthenticated] = useState(AuthAPI.isAuthenticated())

  const NavbarWithRouter = withRouter(Navbar);

  return (
    <AuthContext.Provider value={{
      isAuthenticated,
      setIsAuthenticated
    }}>
      <HashRouter>
        <NavbarWithRouter />
        <main className="container pt-5">
          <Switch>
            <Route path="/login" component={LoginPage} />
            <PrivateRoute path='/characters/:id' component={CharacterPage} />
            <PrivateRoute path='/characters' component={CharactersPage} />
            <PrivateRoute path='/episode/:id' component={EpisodePage} />
            <PrivateRoute path='/episodes' component={EpisodesPage} />
            <Route path="/" component={HomePage} />
          </Switch>
        </main>
      </HashRouter>
      <ToastContainer position={toast.POSITION.BOTTOM_LEFT} />
    </AuthContext.Provider>
  );
}

export default App;
